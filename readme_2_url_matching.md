<h2>url matching for GET/POST request</h2><br/>
<p>when you run the program for first time, a series of folders will be created in 'configuration' directory </p><br/>
<li>it has server.config file that include server configuration you entered at last step</li><br/>
<li>logs directory witch will be used to log requests</li><br/>
<li><b>'urlsOnFly'</b> directory witch will be creating dynamic routing behaviour for GET/POST request</li><br/>
<hr><br/>
<p>when you enter any url like <a href="#"> http://localhost:8083/some/url</a></p><br/>
<li>first servlet check the method type (if it is POST request, it looks in post directory) or (if it is GET request it looks in get directory) </li><br/>
<li>then it check the relative part of URL, in this case: '/some/url'</li><br/>
<li>it convert every first letter after forward slash(/) to upper case</li><br/>
<li>so the '/some/url' becomes 'SomeUrl' and it is the name of directory witch we need to look up inside get or post directory </li><br/>
<li>** if this directory doesn't exist, it will be created for you at first attempt, and a custom instruction will be return</li><br/>
<li>** for better understanding open your browser and enter any url you like</li><br/>
<hr><br/>
<li>inside final directory there should be a file named 'config.txt' for GET request</li><br/>
<li>** for POST request it can be 'config.txt' and/or 'json.schema.txt'</li><br/>
<li>** please keep in mind that they can't be empty (you need to write some rules inside them)</li><br/>
<br/>
