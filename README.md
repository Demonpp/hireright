 <p>this help file consist of seven parts</p><br/>
<li> <a href="readme_1_configure_server.md">how it configure server and run server</a></li><br/>
<li> <a href="readme_2_url_matching.md"> how to configure return data for POST/GET methods</a></li><br/>
<li> <a href="readme_3_param_matching.md">  how to write and match params for GET/POST request</a></li><br/>
<li><a href="readme_4_creating_json_template_for_matching.md"> how to write and match json template for POST Request</a></li><br/>
<li><a href="readme_5_responding_as_json_or_xml.md"> how to return xml/json format with any request</a></li><br/>
<li><a href="readme_6_responding_with_custom_header.md"> how to return custom header with any request</a></li><br/>
<li> logging</li><br/>
<p> -- all logging are shown in console and also saved in 'configuration/logs/networkLog.txt'</p><br/>
<br/>
