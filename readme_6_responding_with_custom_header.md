<h2>responding as with custom header</h2><br/>
<hr>
<p>you could add a file with '.header.txt' into the same directory as your response file</p><br/>
<p>ex.</p><br/>
<p>if your response file is 'someFile.xml'</p><br/>
<p>custom header file should be 'someFile.xml.header.txt'</p><br/>
<hr>
<p>inside '.header.txt'</p><br/>
<p>you can write header key as first line</p><br/>
<p>and header value as the following line</p><br/>
<hr>
<p>ex.</p><br/>
<hr>
<br/>
<pre>
amir-header
exist
amir-header-2
also-exist
</pre>