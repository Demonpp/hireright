<h2>creating json template for matching  in POST request</h2><br/>
<hr><br/>
<p>in POST requests, you can have json checking in addition to param checking</p><br/>
<p>this templates should be placed in 'json.schema.txt' file inside appropriate directory</p><br/>
<p> ** please keep in mind that since creating json template for matching is expensive, at run time the created template will be cached<br/> 
and it will only be change if you change 'json.schema.txt' file</p><br/>
<hr><br/>
<h4>introduction</h4><br/>
<h5>type of json elements</h5><br/>
<li>primary => like: { "a" : 20 } or { "b" : "some text" }<br/> 
<p> ** for template to match you should write **<p><br/>
<p>a [primary] [20]</p><br/>
<p>b [primary] [some text]</p><br/>
<p>** you can use asterisk '*' to match any value</p><br/>
<p>a [primary] [*]</p><br/>
<p>b [primary] [*]</p><br/>
</li><br/>
<li>array => like: { "c" : [1,2,3] } or { "d" : [true, false, true, true]} or { "e" : ["Hello, world", "json files"]}<br/>
<p> ** for template to match you should write **<p><br/>
<p>c [array] [1,2,3]</p><br/>
<p>d [array] [true, false, true, true]</p><br/>
<p>e [array] [Hello,, world,json files]</p><br/>
<p>** you can use asterisk ',,' if you have ',' in your text</p><br/>
<p>** you can use asterisk '*' to match any value</p><br/>
<p>c [array] [*,*,*]</p><br/>
<p>d [array] [*, *, *, *]</p><br/>
<p>e [array] [*,*]</p><br/>
</li><br/>
<br/>
<li>object => like { "f" : { "a" : 6, "b" : [11 , 22] , "c" : { "a" : "some object" } } }<br/>
<p> ** for template to match you should write **<p><br/>
<p>f [object]</p><br/>
<p>f.a [primary] [6]</p><br/>
<p>f.b [array] [11,22]</p><br/>
<p>f.c [object] </p><br/>
<p>f.c.a [primary] [some object] </p><br/>
<p>** you can use asterisk '*' to match any value</p><br/>
<p>f [object]</p><br/>
<p>f.a [primary] [*]</p><br/>
<p>f.b [array] [*,*]</p><br/>
<p>f.c [object] </p><br/>
<p>f.c.a [primary] [*] </p><br/>
</li><br/>
<li>array of object => like { "g" : [ { "a" : 20, "b": [ 1, 2] }, { "a" : 20, "b": [ 3, 4] }] }<br/>
<p> ** for template to match you should write **<p><br/>
<p>g [array][object]</p><br/>
<p>g._.a [primary] [20]</p><br/>
<p>g._.b [array] [*,*]</p><br/>
</li><br/>
<hr><br/>
<li>like what we had in'config.txt'</li><br/>
<li> 1 - the first line should be name of response file with .json/.xml extension</li><br/>
<li> 2 - then write your json rules</li><br/>
<li> 3 - end the rules with '--' without any quotation</li><br/>
<hr><br/>
ex.<br/>
<hr><br/>
<pre>
hi.json
b [array] [true,true,false,true]
c [array] [salamati,, man khobam, hello, ki]
d [primary] [6]
e [object]
e.a [primary] [*]
e.b [array][88, 99]
e.c [array] [*,*,*]
e.d [array] [*]
e.f [object]
e.f.a [primary] [*]
e.h [array][object]
e.h._.a [primary] [*]
e.h._.b [array] [40]
e.h._.c [object] 
a [primary] [salam]
--
hi2.xml
b [array] [true,true,false,true]
c [array] [salamati,, man khobam, hello, ki]
--
</pre>



