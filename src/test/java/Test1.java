import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.hamcrest.Matchers;

import org.junit.BeforeClass;
import org.junit.Test;

import static io.restassured.RestAssured.*;


public class Test1 {

    String URL = "http://localhost:8083";
    int responseStatus = 404;

    @Test
    public void  weatherServiceTest1() {
        Response response = get(URL + "/weather?city-code=1");
        System.out.println(response.asString());
        response.then().statusCode(responseStatus);
        response.then().contentType(ContentType.JSON);
        response.then().body("name", Matchers.is("city 1"));
        response.then().body("todayTemp", Matchers.is(23));
        response.then().header("custom-header-1",Matchers.is("some value"));
        response.then().header("custom-header-2",Matchers.is("another value"));
    }

    @Test
    public void  weatherServiceTest2() {
        Response response = get(URL + "/weather?city-code=2");
        System.out.println(response.asString());
        response.then().statusCode(responseStatus);
        response.then().contentType(ContentType.XML);
        response.then().body("weather.name", Matchers.is("city 2"));
        response.then().body("weather.todayTemp", Matchers.is("13"));
        response.then().header("custom-header-1",Matchers.is("some value"));
        response.then().header("custom-header-2",Matchers.is("another value"));
    }

    @Test
    public void  weatherServiceTest3() {
        Response response = get(URL + "/weather?city-code=3");
        System.out.println(response.asString());
        response.then().statusCode(responseStatus);
        response.then().contentType(ContentType.JSON);
        response.then().body("name", Matchers.is("city 3"));
        response.then().body("todayTemp", Matchers.is(29));
    }

    @Test
    public void  weatherServiceTest4() {
        Response response = get(URL + "/weather?city-code=4");
        System.out.println(response.asString());
        response.then().statusCode(responseStatus);
        response.then().contentType(ContentType.XML);
        response.then().body("weather.name", Matchers.is("city 4"));
        response.then().body("weather.todayTemp", Matchers.is("10"));
    }

    // GEO LOCATION
    @Test
    public void geoLocationTest1() {
        Response response = given().
                contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body("{\"zip\" : 123456 }")
                .when()
                .post(URL + "/Geo-location");
        System.out.println(response.asString());
        response.then().statusCode(404);

        response.then().contentType(ContentType.JSON);
        response.then().body("timeZone", Matchers.is("+1:00"));
        response.then().header("custom-header",Matchers.is("myvalue"));
    }

    @Test
    public void geoLocationTest2() {
        Response response = given().
                contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body("{\"zip\" :234567 }")
                .when()
                .post(URL + "/Geo-location");
        System.out.println(response.asString());
        response.then().statusCode(404);

        response.then().contentType(ContentType.XML);
        response.then().body("time.timeZone", Matchers.is("-3:30"));
        response.then().header("custom-header",Matchers.is("myvalue"));
    }

    @Test
    public void geoLocationTest3() {
        Response response = given().
                contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body("{\"zip\" : 345678 }")
                .when()
                .post(URL + "/Geo-location");
        System.out.println(response.asString());
        response.then().statusCode(404);

        response.then().contentType(ContentType.JSON);
        response.then().body("timeZone", Matchers.is("+5:30"));
    }

    @Test
    public void geoLocationTest4() {
        Response response = given().
                contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body("{\"zip\" :456789 }")
                .when()
                .post(URL + "/Geo-location");
        System.out.println(response.asString());
        response.then().statusCode(404);

        response.then().contentType(ContentType.XML);
        response.then().body("time.timeZone", Matchers.is("+4:30"));
    }

    //----------------

    @Test
    public void getRequestTest1() {
        Response response = get(URL + "/asere?q=8");
        System.out.println(response.asString());
        response.then().statusCode(404);
        response.then().contentType(ContentType.XML);
        response.then().body("note.to", Matchers.is("Tove"));

    }

    @Test
    public void getRequestTest2() {
        Response response = get(URL + "/asere");
        System.out.println(response.asString());
        response.then().statusCode(404);

        response.then().contentType(ContentType.JSON);
        response.then().body("value1", Matchers.is(1));

    }

    @Test
    public void postRequestTest1() {
        Response response = given().
                contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body("{\"b\" : [true,true,false,true],\n" +
                        "\t\"c\" : [\"salamati, man khobam\", \" hello\", \" ki\"]}")
                .when()
                .post(URL + "/amir");
        System.out.println(response.asString());
        response.then().statusCode(404);

        response.then().contentType(ContentType.XML);
        response.then().body("amir.number", Matchers.is("2"));
    }

    @Test
    public void postRequestTest2() {
        Response response = given().
                contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body("{\n" +
                        "\t\"a\": \"salam\",\n" +
                        "\t\"b\" : [true,true,false,true],\n" +
                        "\t\"c\" : [\"salamati, man khobam\", \" hello\", \" ki\"],\n" +
                        "\t\"d\" : 6,\n" +
                        "\t\"e\" : {\n" +
                        "\t\t\"a\": 5,\n" +
                        "\t\t\"b\" : [88,99],\n" +
                        "\t\t\"c\" : [88,99,100],\n" +
                        "\t\t\"d\" : [88],\n" +
                        "\t\t\"f\" : {\n" +
                        "\t\t\t\"a\" : \"c\"\n" +
                        "\t\t},\n" +
                        "\t\t\"h\": [\n" +
                        "\t\t\t{\n" +
                        "\t\t\t\t\"a\" : 33,\n" +
                        "\t\t\t\t\"b\": [40],\n" +
                        "\t\t\t\t\"c\": {}\n" +
                        "\t\t\t}\n" +
                        "\t\t\t]\n" +
                        "\t}\n" +
                        "\t\n" +
                        "\t\n" +
                        "}")
                .when()
                .post(URL + "/amir");
        System.out.println(response.asString());
        response.then().statusCode(404);

        response.then().contentType(ContentType.JSON);
        response.then().body("value1", Matchers.is(1));

    }

    @Test
    public void postRequestTest3() {
        Response response = given()
                .urlEncodingEnabled(true)
                .param("p", "9")
                .param("q", "8")
                .when()
                .post(URL + "/amir");
        System.out.println(response.asString());
        response.then().statusCode(404);

        response.then().contentType(ContentType.XML);
        response.then().body("amir.amir2", Matchers.is("2"));
        response.then().header("amir-header-2",Matchers.is("also-exist"));
        response.then().header("amir-header",Matchers.is("exist"));

    }

}
