package server;

import server.model.ServerDataModel;

import java.io.*;
import java.util.List;

import static helper.manager.FileHierarchyManager.checkStructure;
import static helper.IOHelper.*;
import static helper.IOHelper.getRootFolder;
import static helper.Util.tryParse;
import static server.FilesPath.*;


public class ServerConfiguration extends ServerDataModel {

    private String path;
    private static ServerConfiguration instance = null;
    public static ServerConfiguration getInstance()
    {
        if (instance == null){
            synchronized(ServerConfiguration.class) {
                ServerConfiguration inst = instance;
                if (inst == null) {
                    synchronized(ServerConfiguration.class) {
                        instance = new ServerConfiguration();
                    }
                }
            }
        }
        return instance;
    }
    public void init() {
        init(null,null,null,null,null);
    }

    public void init( Object port,
                     Object responseDelayed, Object responseDelayTime,
                     Object customResponseStatus, Object responseStatus) {

        this.portNumber = (port == null) ? DEFAULT_PORT_NUMBER_KEYWORD : (int)port;
        this.responseDelayed = (responseDelayed==null) ? DEFAULT_BOOLEAN_VALUE : (Boolean) responseDelayed;
        this.responseDelayTime = (responseDelayTime == null) ? DEFAULT_RESPONSE_DELAY_TIME_KEYWORD : (int)responseDelayTime;
        this.customResponseStatus = (customResponseStatus==null) ? DEFAULT_BOOLEAN_VALUE : (Boolean) customResponseStatus;
        this.responseStatus = (responseStatus == null) ? DEFAULT_RESPONSE_CUSTOM_STATUES_VALUE_KEYWORD : (int)responseStatus;

        try {
            checkStructure(this.path);
            setServerConfiguration();
        } catch (IOException e) {
            e.printStackTrace();
        }



    }

    private ServerConfiguration() {
        path = getRootFolder().getAbsolutePath();
    }

    private void setServerConfiguration()  {
        String filePath = this.path+SERVER_FILE_RELATIVE_PATH;

        try {
            String [] defaultValues = {
                    PORT_KEYWORD,String.valueOf(this.portNumber),KEYWORD_SEPARATOR,
                    RESPONSE_DELAY_KEYWORD, (this.responseDelayed ? ENABLE_KEYWORD : DISABLE_KEYWORD) ,KEYWORD_SEPARATOR,
                    RESPONSE_DELAY_TIME_KEYWORD,String.valueOf(this.responseDelayTime),KEYWORD_SEPARATOR,
                    RESPONSE_CUSTOM_STATUES_KEYWORD, (this.customResponseStatus ? ENABLE_KEYWORD : DISABLE_KEYWORD) ,KEYWORD_SEPARATOR,
                    RESPONSE_CUSTOM_STATUES_VALUE_KEYWORD,String.valueOf(this.responseStatus),KEYWORD_SEPARATOR
            };

            List<String> fileLines = forceToCreateFileData(filePath,defaultValues);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void getServerConfiguration()  {
        String filePath = this.path+SERVER_FILE_RELATIVE_PATH;

        try {
            String [] defaultValues = {
                    PORT_KEYWORD,String.valueOf(DEFAULT_PORT_NUMBER_KEYWORD),KEYWORD_SEPARATOR,
                    RESPONSE_DELAY_KEYWORD,DEFAULT_RESPONSE_DELAY_KEYWORD,KEYWORD_SEPARATOR,
                    RESPONSE_DELAY_TIME_KEYWORD,String.valueOf(DEFAULT_RESPONSE_DELAY_TIME_KEYWORD),KEYWORD_SEPARATOR,
                    RESPONSE_CUSTOM_STATUES_KEYWORD,DEFAULT_RESPONSE_CUSTOM_STATUES_KEYWORD,KEYWORD_SEPARATOR,
                    RESPONSE_CUSTOM_STATUES_VALUE_KEYWORD,String.valueOf(DEFAULT_RESPONSE_CUSTOM_STATUES_VALUE_KEYWORD),
                    KEYWORD_SEPARATOR
            };

            List<String> fileLines = getOrCreateFileData(filePath,defaultValues);
            boolean readValue = false;
            String readType =KEYWORD_SEPARATOR;

            for (String fl : fileLines) {
                if(readValue) {
                    readValue = false;
                    switch (readType) {
                        case KEYWORD_SEPARATOR:
                            readValue = false;
                            break;
                        case PORT_KEYWORD:
                            fl = fl.trim();
                            if(!fl.isEmpty() ) this.portNumber = tryParse(fl,DEFAULT_PORT_NUMBER_KEYWORD);
                            else this.portNumber = DEFAULT_PORT_NUMBER_KEYWORD;
                            break;
                        case RESPONSE_DELAY_KEYWORD:
                            fl = fl.trim();
                            if(!fl.isEmpty() ) this.responseDelayed = tryParse(fl,DISABLE_KEYWORD,true);
                            else this.responseDelayed = false;
                            break;
                        case RESPONSE_DELAY_TIME_KEYWORD:
                            fl = fl.trim();
                            if(!fl.isEmpty() ) this.responseDelayTime = tryParse(fl,DEFAULT_RESPONSE_DELAY_TIME_KEYWORD);
                            else this.responseDelayTime = DEFAULT_RESPONSE_DELAY_TIME_KEYWORD;
                            break;
                        case RESPONSE_CUSTOM_STATUES_KEYWORD:
                            fl = fl.trim();
                            if(!fl.isEmpty() ) this.customResponseStatus = tryParse(fl,DEFAULT_RESPONSE_CUSTOM_STATUES_KEYWORD,true);
                            else this.customResponseStatus = false;
                            break;
                        case RESPONSE_CUSTOM_STATUES_VALUE_KEYWORD:
                            fl = fl.trim();
                            if(!fl.isEmpty() ) this.responseStatus = tryParse(fl,DEFAULT_RESPONSE_CUSTOM_STATUES_VALUE_KEYWORD);
                            else this.responseStatus = DEFAULT_RESPONSE_CUSTOM_STATUES_VALUE_KEYWORD;
                            break;
                    }
                } else {
                    readValue = true;
                    switch (fl){
                        case KEYWORD_SEPARATOR:
                            readValue = false;
                            break;
                        case PORT_KEYWORD:
                            readType = PORT_KEYWORD;
                            break;
                        case RESPONSE_DELAY_KEYWORD:
                            readType = RESPONSE_DELAY_KEYWORD;
                            break;
                        case RESPONSE_DELAY_TIME_KEYWORD:
                            readType = RESPONSE_DELAY_TIME_KEYWORD;
                            break;
                        case RESPONSE_CUSTOM_STATUES_KEYWORD:
                            readType = RESPONSE_CUSTOM_STATUES_KEYWORD;
                            break;
                        case RESPONSE_CUSTOM_STATUES_VALUE_KEYWORD:
                            readType = RESPONSE_CUSTOM_STATUES_VALUE_KEYWORD;
                            break;
                    }
                }            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
