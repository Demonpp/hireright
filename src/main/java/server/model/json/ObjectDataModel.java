package server.model.json;

import server.model.json.base.JsonArrayBaseClass;
import server.model.json.base.JsonBaseClass;

import java.util.ArrayList;
import java.util.List;

public class ObjectDataModel extends JsonArrayBaseClass {
    private List<PrimaryDataModel> primaryDataList;
    private List<PrimaryArrayDataModel> primaryArrayDataList;
    private List<ObjectDataModel> objectDataList;
    private List<ObjectArrayDataModel> objectArrayDataList;

    public ObjectDataModel() {
        this.primaryDataList = new ArrayList<>();
        this.primaryArrayDataList = new ArrayList<>();
        this.objectDataList = new ArrayList<>();
        this.objectArrayDataList = new ArrayList<>();
    };



    public ObjectDataModel(String name, List<PrimaryDataModel> primaryDataList, List<PrimaryArrayDataModel> primaryArrayDataList,
                           List<ObjectDataModel> objectDataList, List<ObjectArrayDataModel> objectArrayDataList) {
        this.name = name;
        this.primaryDataList = primaryDataList;
        this.primaryArrayDataList = primaryArrayDataList;
        this.objectDataList = objectDataList;
        this.objectArrayDataList = objectArrayDataList;
    }

    public List<ObjectArrayDataModel> getObjectArrayDataList() {
        return objectArrayDataList;
    }

    public List<ObjectDataModel> getObjectDataList() {
        return objectDataList;
    }

    public List<PrimaryArrayDataModel> getPrimaryArrayDataList() {
        return primaryArrayDataList;
    }

    public List<PrimaryDataModel> getPrimaryDataList() {
        return primaryDataList;
    }

    public void setObjectArrayDataList(List<ObjectArrayDataModel> objectArrayDataList) {
        this.objectArrayDataList = objectArrayDataList;
    }

    public void setObjectDataList(List<ObjectDataModel> objectDataList) {
        this.objectDataList = objectDataList;
    }

    public void setPrimaryArrayDataList(List<PrimaryArrayDataModel> primaryArrayDataList) {
        this.primaryArrayDataList = primaryArrayDataList;
    }

    public void setPrimaryDataList(List<PrimaryDataModel> primaryDataList) {
        this.primaryDataList = primaryDataList;
    }

    @Override
    public int getSize() {
        return this.primaryDataList.size() +
                this.primaryArrayDataList.size()+
                this.objectDataList.size()+
                this.objectArrayDataList.size();
    }

    public void clear() {
        this.primaryDataList.clear();
        this.primaryArrayDataList.clear();
        this.objectDataList.clear();
        this.objectArrayDataList.clear();
    }

    public void add(PrimaryDataModel data) {
        this.primaryDataList.add(data);
    }

    public void add(PrimaryArrayDataModel data) {
        this.primaryArrayDataList.add(data);
    }

    public void add(ObjectDataModel data) {
        this.objectDataList.add(data);
    }

    public void add(ObjectArrayDataModel data) {
        this.objectArrayDataList.add(data);
    }

    public void add(JsonBaseClass jbc) {
        if(jbc instanceof ObjectDataModel) {
            this.add((ObjectDataModel) jbc);
        } else if(jbc instanceof PrimaryArrayDataModel) {
            this.add((PrimaryArrayDataModel) jbc);
        } else if(jbc instanceof ObjectArrayDataModel) {
            this.add((ObjectArrayDataModel) jbc);
        } else if(jbc instanceof PrimaryDataModel) {
            this.add((PrimaryDataModel) jbc);
        }
    }

}
