package server.model.json.helper;

import java.util.List;

public class JsonSchemaCacher {
    private String path;
    private String hashChecksum;
    private List<JsonFileDataStructure> schemas;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getHashChecksum() {
        return hashChecksum;
    }

    public void setHashChecksum(String hashChecksum) {
        this.hashChecksum = hashChecksum;
    }

    public List<JsonFileDataStructure> getSchemas() {
        return schemas;
    }

    public void setSchemas(List<JsonFileDataStructure> schemas) {
        this.schemas = schemas;
    }
}
