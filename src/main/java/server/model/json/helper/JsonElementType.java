package server.model.json.helper;

public enum  JsonElementType {
    PRIMARY, ARRAY, OBJECT, ARRAY_OF_OBJECT
}
