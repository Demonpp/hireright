package server.model.json.helper;

import server.model.json.ObjectDataModel;
import server.model.json.base.JsonBaseClass;

import java.util.Comparator;
import java.util.List;

public class JsonFileDataStructure {
    private String responseFileName;
    private ObjectDataModel schema;
    private List<JsonBaseClass> flatSchemas;

    public JsonFileDataStructure(String responseFileName, List<JsonBaseClass> flatSchemas) {
        this.responseFileName = responseFileName;
        this.flatSchemas = flatSchemas;
        this.schema = new ObjectDataModel();
    }



    private void sort() {
        this.flatSchemas.sort(Comparator.comparing(JsonBaseClass::getLevel).reversed());
    }

    public String getResponseFileName() {
        return responseFileName;
    }

    public void setResponseFileName(String responseFileName) {
        this.responseFileName = responseFileName;
    }

    public ObjectDataModel getSchema() {
        return schema;
    }

    public void calcSchema() {
        this.sort();
        for(JsonBaseClass j : flatSchemas) {
            if(j.isItFlatStructure())
                this.addElementToParent(j);
        }
        schema.setName("root");
    }

    private void addElementToParent(JsonBaseClass jbc) {
        jbc.setItFlatStructure(false);
        if(jbc.getLevel()==0) {
            schema.add(jbc);
           return;
        }
        int index = findParentIndexByFullName(jbc.getParentName());
        if(index == -1) return;

        ((ObjectDataModel)this.flatSchemas.get(index)).add(jbc);


    }

    private int findParentIndexByFullName(String fullName) {
        for( int i =0; i< this.flatSchemas.size(); i++) {
            if(this.flatSchemas.get(i).getFullName().equals(fullName))
                return i;
        }
        return -1;
    }

}
