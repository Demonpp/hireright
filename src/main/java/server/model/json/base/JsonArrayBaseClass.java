package server.model.json.base;

public abstract class JsonArrayBaseClass extends JsonBaseClass {

    public abstract int getSize();
}
