package server.model.json.base;

public class JsonBaseClass {
    protected String name;
    protected boolean isValueImportant;
    protected boolean isItFlatStructure;

    protected int level;
    protected String fullName;
    protected String parentName;
    protected boolean isParentArrayOfObject;

    public JsonBaseClass() {
        this.isItFlatStructure = true;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public boolean isValueImportant() {
        return isValueImportant;
    }

    public void setValueImportant(boolean valuesImportant) {
        isValueImportant = valuesImportant;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public boolean isParentArrayOfObject() {
        return isParentArrayOfObject;
    }

    public void setParentArrayOfObject(boolean parentArrayOfObject) {
        isParentArrayOfObject = parentArrayOfObject;
    }

    public boolean isItFlatStructure() {
        return isItFlatStructure;
    }

    public void setItFlatStructure(boolean itFlatStructure) {
        isItFlatStructure = itFlatStructure;
    }
}
