package server.model.json;

import server.model.json.base.JsonBaseClass;

public class PrimaryDataModel extends JsonBaseClass {
    private String value;
    public PrimaryDataModel() {}
    public PrimaryDataModel(String name, String value, boolean isValueImportant) {
        this.name = name;
        this.value = value;
        this.isValueImportant = isValueImportant;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
