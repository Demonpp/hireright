package server.model.json;

import server.model.json.base.JsonArrayBaseClass;

import java.util.ArrayList;
import java.util.List;

public class PrimaryArrayDataModel extends JsonArrayBaseClass {

    private List<String> values;

    public PrimaryArrayDataModel() {
        this.values = new ArrayList<>();
    }
    public PrimaryArrayDataModel(String name, List<String> values, boolean isValueImportant, int lengthShouldBe) {
        this.name = name;
        this.values = values;
        this.isValueImportant = isValueImportant;
    }

    public List<String> getValues() {
        return values;
    }

    public void setValues(List<String> values) {
        this.values = values;
    }

    @Override
    public int getSize() {
        return this.values.size();
    }
    public void addToValues(String value) {
        this.values.add(value);
    }

}
