package server.model;

public class ServerDataModel {
    protected int portNumber;
    protected boolean urlCaching;
    protected boolean responseDelayed;
    protected int responseDelayTime;
    protected boolean customResponseStatus;
    protected int responseStatus;

    public int getPortNumber() {
        return this.portNumber;
    }
    public void setPortNumber(int portNumber) {
        this.portNumber = portNumber;
    }

    public boolean getUrlCaching() {
        return this.urlCaching;
    }
    public void setUrlCaching(boolean urlCaching) {
        this.urlCaching = urlCaching;
    }

    public boolean setResponseDelayed() {
        return this.responseDelayed;
    }
    public void setResponseDelayed(boolean responseDelayed) {
        this.responseDelayed = responseDelayed;
    }

    public int getResponseDelayTime() {
        return this.responseDelayTime;
    }
    public void setResponseDelayTime(int responseDelayTime) {
        this.responseDelayTime = responseDelayTime;
    }

    public boolean getCustomResponseStatus() {
        return this.customResponseStatus;
    }
    public void setCustomResponseStatus(boolean customResponseStatus) {
        this.customResponseStatus = customResponseStatus;
    }

    public int getResponseStatus() {
        return this.responseStatus;
    }
    public void setResponseStatus(int responseStatus) {
        this.responseStatus = responseStatus;
    }
}
