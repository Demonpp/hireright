package server.model;

import java.util.List;

public class ResponseDataModel {
    private String contentType;
    private List<String> content;
    private List<String> customHeader;
    private List<String> customHeaderValues;
    private int status;
    private String resourceName;
    private String errorMessage;

    public ResponseDataModel() {}
    public ResponseDataModel(int status, String errorMessage,String contentType) {
        this.status = status;
        this.errorMessage = errorMessage;
        this.contentType = contentType;
    }
    public ResponseDataModel(String contentType, List<String> content, List<String> customHeader, List<String> customHeaderValues)  {
        this.contentType = contentType;
        this.content = content;
        this.customHeader = customHeader;
        this.customHeaderValues = customHeaderValues;
    }

    public String getContentType() {return this.contentType;}
    public void setContentType(String contentType) {this.contentType = contentType;}
    public List<String> getContent() {return this.content;}
    public void setContent(List<String> content) {this.content = content;}
    public List<String> getCustomHeader() {return this.customHeader;}
    public void setCustomHeader(List<String> customHeader) {this.customHeader = customHeader;}
    public List<String> getCustomHeaderValues() {return this.customHeaderValues;}
    public void setCustomHeaderValues(List<String> customHeaderValues) {this.customHeaderValues = customHeaderValues;}
    public int getStatus() {return this.status;}
    public void setStatus(int status) {this.status = status;}
    public String getResourceName() {return this.resourceName;}
    public void setResourceName(String resourceName) {this.resourceName = resourceName;}
    public String getErrorMessage() {return this.errorMessage;}
    public void setErrorMessage(String errorMessage) {this.errorMessage = errorMessage;}
}
