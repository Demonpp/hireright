package server;

import org.apache.catalina.LifecycleException;
import org.apache.catalina.WebResourceRoot;
import org.apache.catalina.WebResourceSet;
import org.apache.catalina.core.StandardContext;
import org.apache.catalina.startup.Tomcat;
import org.apache.catalina.webresources.DirResourceSet;
import org.apache.catalina.webresources.EmptyResourceSet;
import org.apache.catalina.webresources.StandardRoot;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;

import static helper.IOHelper.createTempDirectory;
import static helper.IOHelper.getRootFolder;

public class Emulator {

    private static Tomcat tomcat;

    private static void startServer(String path, int port) throws LifecycleException, IOException {
        if(tomcat == null) {
            System.setProperty("org.apache.catalina.startup.EXIT_ON_INIT_FAILURE", "true");
            tomcat = new Tomcat();
            Path tempPath =  createTempDirectory("tomcat-base-dir");
            tomcat.setBaseDir(tempPath.toString());

            tomcat.setPort(port);

            File webContentFolder = new File(path, "src/main/webapp/");
            if (!webContentFolder.exists()) {
                webContentFolder = Files.createTempDirectory("default-doc-base").toFile();
            }
            StandardContext ctx = (StandardContext) tomcat.addWebapp("", webContentFolder.getAbsolutePath());
            //Set execution independent of current thread context classloader (compatibility with exec:java mojo)
            ctx.setParentClassLoader(Emulator.class.getClassLoader());

            // Declare an alternative location for your "WEB-INF/classes" dir
            File additionWebInfClassesFolder = new File(path, "target/classes");
            WebResourceRoot resources = new StandardRoot(ctx);

            WebResourceSet resourceSet;
            if (additionWebInfClassesFolder.exists()) {
                resourceSet = new DirResourceSet(resources, "/WEB-INF/classes",
                        additionWebInfClassesFolder.getAbsolutePath(), "/");
                System.out.println("loading WEB-INF resources from as '" +
                        additionWebInfClassesFolder.getAbsolutePath() + "'");
            } else {
                resourceSet = new EmptyResourceSet(resources);
            }
            resources.addPreResources(resourceSet);
            ctx.setResources(resources);
        }

        System.out.println("starting server @ port "+port);
        tomcat.start();
        System.out.println("started server @ http://"+tomcat.getServer().getAddress()+":"+port);
    }

    public static void main(String[] args) throws Exception {
        File rootDir = getRootFolder();
        BufferedReader reader =
                new BufferedReader(new InputStreamReader(System.in));


        System.out.println("please enter the server port number:");
        System.out.println("ex. : 8081");
        System.out.println(":");
        int port =Integer.parseInt(reader.readLine());

        System.out.println("do you want to have some delay on response?:");
        System.out.println("Y/N");
        System.out.println(":");

        boolean responseDelay = reader.readLine().toLowerCase().equals("y");

        int responseDelayTime = 0;
        if(responseDelay) {
            System.out.println("please enter delay time in milliseconds");
            System.out.println("ex. 5000");
            System.out.println(":");

            responseDelayTime= Integer.parseInt(reader.readLine());
        }

        System.out.println("do you want to have custom response status?:");
        System.out.println("Y/N");
        System.out.println(":");

        boolean responseStatus = reader.readLine().toLowerCase().equals("y");
        int responseStatusNumber = 200;
        if(responseStatus) {
            System.out.println("please enter response status number");
            System.out.println("ex. 404");
            System.out.println(":");

            responseStatusNumber= Integer.parseInt(reader.readLine());
        }


        System.out.println("ok, server is initializing...");
        System.out.println("you can have type the following commands to quiet the program, or stop and start the server");
        System.out.println("kill --> stop the server");
        System.out.println("start --> start the server");
        System.out.println("q --> stop the server and exit the program");


        ServerConfiguration serverConfiguration = ServerConfiguration.getInstance();

        serverConfiguration.init(port,  responseDelay, responseDelayTime,
                responseStatus, responseStatusNumber);


        startServer(rootDir.getAbsolutePath(),serverConfiguration.getPortNumber());

        while (true){
            String name = reader.readLine();
            if(name.equals("kill")) {
                tomcat.stop();
                System.out.println("server stopped");
            } else if(name.equals("q")) {
                tomcat.stop();
                System.out.println("server stopped");
                break;
            } else if(name.equals("start")) {
                startServer(rootDir.getAbsolutePath(),serverConfiguration.getPortNumber());
            } else {
                System.out.println("echo: "+name+ serverConfiguration.getPortNumber());
            }
        }

    }
}
