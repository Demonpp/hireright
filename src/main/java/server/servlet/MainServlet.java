package server.servlet;

import helper.manager.RequestManager;
import server.ServerConfiguration;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet(
        name = "MainServlet",
        urlPatterns = {"/*"}
)
public class MainServlet extends HttpServlet {
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ServerConfiguration serverConfiguration = ServerConfiguration.getInstance();
        serverConfiguration.getServerConfiguration();

        RequestManager rm = new RequestManager(request,response,"get",serverConfiguration);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        ServerConfiguration serverConfiguration = ServerConfiguration.getInstance();
        serverConfiguration.getServerConfiguration();

        RequestManager rm = new RequestManager(request,response,"post",serverConfiguration);
    }




}
