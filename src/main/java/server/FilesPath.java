package server;

public class FilesPath {
    public static final String SEPARATOR = "\\";

    public static final String SERVER_CONFIGURATION_DIRECTORY = SEPARATOR+"configuration";
    public static final String SERVER_FILE = SEPARATOR+"server.config";
    public static final String URLS_ON_FLY_DIRECTORY = SEPARATOR+"urlsOnFly";
    public static final String POST_REQUEST_DIRECTORY = SEPARATOR+"post";
    public static final String GET_REQUEST_DIRECTORY = SEPARATOR+"get";

    public static final String LOG_DIRECTORY = SERVER_CONFIGURATION_DIRECTORY+SEPARATOR+"logs";
    public static final String LOG_FILE = LOG_DIRECTORY+SEPARATOR+"networkLog.txt";


    public static final String SERVER_FILE_RELATIVE_PATH = SERVER_CONFIGURATION_DIRECTORY+SERVER_FILE;

    public static final String URLS_ON_FLY_DIRECTORY_RELATIVE_PATH  = SERVER_CONFIGURATION_DIRECTORY+URLS_ON_FLY_DIRECTORY;

    public static final String POST_REQUEST_DIRECTORY_RELATIVE_PATH  = URLS_ON_FLY_DIRECTORY_RELATIVE_PATH+POST_REQUEST_DIRECTORY;

    public static final String GET_REQUEST_DIRECTORY_RELATIVE_PATH  = URLS_ON_FLY_DIRECTORY_RELATIVE_PATH+GET_REQUEST_DIRECTORY;


    public static final String CONFIG_FILE = "config.txt";
    public static final String CONFIG_FILE_HEADER = ".header.txt";
    public static final String JSON_SCHEMA_FILE = "json.schema.txt";

    public static final Boolean DEFAULT_BOOLEAN_VALUE = false;

    public static final String DISABLE_KEYWORD = "disable";
    public static final String ENABLE_KEYWORD = "enable";

    public static final String KEYWORD_SEPARATOR = "--";
    public static final String PORT_KEYWORD = "port";
    public static final int DEFAULT_PORT_NUMBER_KEYWORD = 8080;
    public static final String RESPONSE_DELAY_KEYWORD = "response_delay";
    public static final String DEFAULT_RESPONSE_DELAY_KEYWORD = DISABLE_KEYWORD;
    public static final String RESPONSE_DELAY_TIME_KEYWORD = "response_delay_time_ms";
    public static final int DEFAULT_RESPONSE_DELAY_TIME_KEYWORD = 10;
    public static final String RESPONSE_CUSTOM_STATUES_KEYWORD = "response_custom_status";
    public static final String DEFAULT_RESPONSE_CUSTOM_STATUES_KEYWORD = DISABLE_KEYWORD;
    public static final String RESPONSE_CUSTOM_STATUES_VALUE_KEYWORD = "response_custom_status_value";
    public static final int DEFAULT_RESPONSE_CUSTOM_STATUES_VALUE_KEYWORD = 404;





}
