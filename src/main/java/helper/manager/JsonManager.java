package helper.manager;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import server.model.json.ObjectArrayDataModel;
import server.model.json.ObjectDataModel;
import server.model.json.PrimaryArrayDataModel;
import server.model.json.PrimaryDataModel;
import server.model.json.base.JsonBaseClass;
import server.model.json.helper.JsonElementType;
import server.model.json.helper.JsonFileDataStructure;
import server.model.json.helper.JsonSchemaCacher;

import java.io.File;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static helper.IOHelper.*;
import static helper.Util.getFileChecksum;
import static server.FilesPath.KEYWORD_SEPARATOR;

 public class JsonManager {
    private static JsonManager instance = null;

    private List<JsonSchemaCacher> schemaCachers;

    private JsonManager() {
        this.schemaCachers = new ArrayList<>();
    }

    public static JsonManager getInstance(){
        if(instance == null) {
            instance = new JsonManager();
        }
        return instance;
}

    private boolean doesJsonObjectsMatch(JsonElement jsonTree , ObjectDataModel objectDataModel) {
        if(jsonTree.isJsonObject()) {
            JsonObject jsonObject = jsonTree.getAsJsonObject();

            for( PrimaryDataModel pdm : objectDataModel.getPrimaryDataList()) {
                JsonElement jElement = jsonObject.get(pdm.getName());
                if(jElement == null) {  return false; }
                else if(! jElement.isJsonPrimitive()) { return false; }
                if(!pdm.getValue().equals("*") &&
                        !jElement.getAsString().equals(pdm.getValue()))
                { return false; }
            }

            for(PrimaryArrayDataModel  padm : objectDataModel.getPrimaryArrayDataList()) {
                JsonElement jElement = jsonObject.get(padm.getName());
                if(jElement == null) { return false; }
                else if(! jElement.isJsonArray()) {
                    return false; }
                if(padm.isValueImportant()) {
                    if ( padm.getSize() != jElement.getAsJsonArray().size() ) {

                        return false;
                    }

                    for(int i =0 ; i <jElement.getAsJsonArray().size(); i++) {
                        JsonElement temp= jElement.getAsJsonArray().get(i);
                        if(temp.isJsonPrimitive()) {
                            if(! temp.getAsString().equals(padm.getValues().get(i))) {
                                return false;
                            }
                        } else {
                            return false;
                        }
                    }
                }
            }

            for(ObjectDataModel odm : objectDataModel.getObjectDataList()) {
                doesJsonObjectsMatch(jsonObject ,odm);
            }

            for(ObjectArrayDataModel oadm : objectDataModel.getObjectArrayDataList()) {
                doesJsonObjectsMatch(jsonObject ,oadm);
            }


        } else {
            return false;
        }

        return true;
    }

    public String getResource(String jsonBody, String path) {
        String resourceFileName = "";
        List<JsonFileDataStructure> schemas;
        try {
            schemas = this.getSchemas(path);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return resourceFileName;
        } catch (IOException e) {
            e.printStackTrace();
            return resourceFileName;
        }



        JsonParser jsonParser = new JsonParser();
        JsonElement jsonTree = null;
        try {
            jsonTree = jsonParser.parse(jsonBody);
        } catch (Exception e) {
            return "";
        }



        if(jsonTree.isJsonObject()) {
            for(JsonFileDataStructure schema : schemas) {
                ObjectDataModel odm = schema.getSchema();
                if(doesJsonObjectsMatch(jsonTree,odm)) {
                    resourceFileName = schema.getResponseFileName();
                    break;
                }
            }
        }
        return resourceFileName;
    }

    public List<JsonFileDataStructure> getSchemas(String path) throws NoSuchAlgorithmException, IOException {
        List<JsonFileDataStructure> schemas = new ArrayList<>();

        if(isFileOrDirectoryExist(path)) {
            //Create checksum for this file
            File file = getOrCreateDirectory(path);

            //Use MD5 algorithm
            MessageDigest md5Digest = MessageDigest.getInstance("MD5");

            //Get the checksum
            String checksum = getFileChecksum(md5Digest, file);


            Optional<JsonSchemaCacher> jsc = findJsonSchemaByPath(schemaCachers,path);
            if(jsc.isPresent()) {
                if(jsc.get().getHashChecksum().equals(checksum)) {
                    schemas = jsc.get().getSchemas();
                } else { // schema has been changed
                    schemaCachers.remove(jsc.get()); // remove old schema from cache
                }
            } else {
                schemas = addNewSchemaToCacher(path,checksum);
            }
        }
        return schemas;
    }

    private Optional<JsonSchemaCacher> findJsonSchemaByPath(final List<JsonSchemaCacher> list, final String path) {
        return list.stream().filter(p -> p.getPath().equals(path)).findAny();
    }
    private List<JsonFileDataStructure> addNewSchemaToCacher(String path, String checksum ) {
        List<JsonFileDataStructure> schemas = proceedSchema(path);
        JsonSchemaCacher jsc = new JsonSchemaCacher();
        jsc.setHashChecksum(checksum);
        jsc.setPath(path);
        jsc.setSchemas(schemas);
        this.schemaCachers.add(jsc );

        return schemas;
    }

    private List<JsonFileDataStructure> proceedSchema(String path) {
        List<JsonFileDataStructure> schemas = new ArrayList<>();

        try {
            List<String> content = readFromFile(path);

            boolean reachedSeparator = true;

            String fileName ="";
            ObjectDataModel schema = new ObjectDataModel();

            List<JsonBaseClass> flatSchemas = new ArrayList<>();

            for(String line : content) {
                if(line.equals(KEYWORD_SEPARATOR)) {
                    reachedSeparator = true;
                    JsonFileDataStructure jfds = new JsonFileDataStructure(fileName, flatSchemas);
                    jfds.calcSchema();

                    schemas.add(jfds);
                    fileName = "";
                    schema.clear();
                } else if(reachedSeparator) {
                    fileName = line;
                    reachedSeparator = false;
                } else {

                    line = line.trim();
                    if(line.contains("[primary]")) {

                        PrimaryDataModel jbc = (PrimaryDataModel) getAttributeName(line,JsonElementType.PRIMARY);
                        String expectedValue =line.substring( line.lastIndexOf('[')+1);
                        expectedValue = expectedValue.substring(0,expectedValue.indexOf(']'));

                        jbc.setValueImportant(!expectedValue.equals("*"));
                        jbc.setValue(expectedValue);


                        flatSchemas.add(jbc);

                    } else if(line.contains("[array]") && line.contains("[object]")) {
                        ObjectArrayDataModel jbc = (ObjectArrayDataModel) getAttributeName(line,JsonElementType.ARRAY_OF_OBJECT);

                        flatSchemas.add(jbc);

                    }else if(line.contains("[array]")) {
                        PrimaryArrayDataModel jbc = (PrimaryArrayDataModel) getAttributeName(line,JsonElementType.ARRAY);


                        String expectedValue =line.substring( line.lastIndexOf('[')+1);
                        expectedValue = expectedValue.substring(0,expectedValue.indexOf(']'));


                        if(expectedValue.equals("*")) {
                            jbc.setValueImportant(false);
                        } else {
                            jbc.setValueImportant(true);

                            expectedValue = expectedValue.replace(",,","[__]");
                            String[] values = expectedValue.split("\\,");
                            for(String v : values) {
                                v = v.replace("[__]",",");
                                jbc.addToValues(v);
                            }
                        }



                        flatSchemas.add(jbc);

                    }else if(line.contains("[object]")) {
                        ObjectDataModel jbc = (ObjectDataModel) getAttributeName(line,JsonElementType.OBJECT);

                        flatSchemas.add(jbc);

                    }
                }

            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return schemas;
    }

    private JsonBaseClass getAttributeName(String line, JsonElementType jst) {
        JsonBaseClass jbc;
        if(jst == JsonElementType.OBJECT) {
            jbc = new ObjectDataModel();
        } else if(jst == JsonElementType.ARRAY_OF_OBJECT) {
            jbc = new ObjectArrayDataModel();
        } else if(jst == JsonElementType.ARRAY) {
            jbc = new PrimaryArrayDataModel();
        } else {
            jbc = new PrimaryDataModel();
        }
        String[] names =  {"",""};
        line = line.substring(0,line.indexOf('['));
        line = line.replace(" ","");

        jbc.setFullName(line);

        String[] temp =line.split("\\.");
        int i =0;
        for(; i<temp.length-1;i++) {
            if(names[0].length() >0) names[0] += ".";
            names[0] += temp[i];
        }

        if(names[0].endsWith("._")) {
            jbc.setParentArrayOfObject(true);
            names[0] = names[0].substring(0,names[0].lastIndexOf("._"));
        }

        names[1] = temp[temp.length-1];
        jbc.setParentName(names[0]);
        jbc.setName(names[1]);
        jbc.setLevel(i);

        return jbc;
    }
}
