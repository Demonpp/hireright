package helper.manager;

import java.io.IOException;

import static helper.IOHelper.getOrCreateDirectory;
import static helper.IOHelper.getOrCreateFileData;
import static server.FilesPath.*;

public class FileHierarchyManager {
    public static void checkStructure(String path) throws IOException {
        getOrCreateDirectory(path+ SERVER_CONFIGURATION_DIRECTORY);

        getOrCreateDirectory(path+URLS_ON_FLY_DIRECTORY_RELATIVE_PATH);

        getOrCreateDirectory(path+POST_REQUEST_DIRECTORY_RELATIVE_PATH);

        getOrCreateDirectory(path+GET_REQUEST_DIRECTORY_RELATIVE_PATH);

        // log directory
        getOrCreateDirectory(path+LOG_DIRECTORY);
        getOrCreateFileData(path+LOG_FILE);

    }
}
