package helper.manager;

import server.ServerConfiguration;
import server.model.ResponseDataModel;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.*;
import java.util.*;

import static helper.IOHelper.*;
import static helper.Util.convertUriToPath;
import static server.FilesPath.*;

public class RequestManager {
    private boolean response_delay;
    private int response_delay_time_ms;
    private boolean response_custom_status;
    private int response_custom_status_value;

    public RequestManager(HttpServletRequest request, HttpServletResponse response, String requestMethod,
                          ServerConfiguration serverConfiguration) throws IOException, ServletException {
        this.initFromConfigFile();

        if(request.getRequestURI().toLowerCase().equals("/static/default.css")) {
            List<String> lines = readFromFile(getCssResource());
            PrintWriter out = response.getWriter();
            for(String line: lines) {
                out.println(line);
            }
        } else { // if not asking for /static/default.css file
            PrintWriter out = response.getWriter();
            String[] splitted = request.getRequestURI().split("[?]");
            if(splitted[0]!=null) { // url is not root
                String dirPath = convertUriToPath(splitted[0],requestMethod);

                responseWriterLogger("request method  = "+requestMethod);
                responseWriterLogger("url  = "+splitted[0]);

                Enumeration paramNames = request.getParameterNames();
                while(paramNames.hasMoreElements()) {
                    String pName = (String)paramNames.nextElement();

                    responseWriterLogger(pName+
                            " parameter = "+request.getParameter(pName));
                }


                if(isFileOrDirectoryExist(dirPath+SEPARATOR+CONFIG_FILE)) {
                    List<String> configFile = readFromFile(dirPath+SEPARATOR+CONFIG_FILE);
                    if(configFile.size() == 0 && requestMethod.equals("get")) {
                        String message = resourceNotFoundMessageGenerator(dirPath);
                        resourceNotFound(response,message);
                        return;
                    }

                    List<String> paramList = Collections.list(request.getParameterNames());
                    List<String> paramValueList = new ArrayList<>();

                    for(String p : paramList) {
                        String[] paramValues = request.getParameterValues(p);
                        paramValueList.add(paramValues.length > 0 ? paramValues[0] : null);
                    }

                    if(requestMethod.equals("post") && paramValueList.size()==0) {
                        StringBuilder buffer = new StringBuilder();
                        BufferedReader reader = request.getReader();
                        String line;
                        while ((line = reader.readLine()) != null) {
                            buffer.append(line);
                        }

                        String data = buffer.toString();

                        if(buffer.length()>0) {
                            JsonManager jsm =  JsonManager.getInstance();
                            String resourceName = jsm.getResource(data,dirPath+SEPARATOR+JSON_SCHEMA_FILE);
                            if(!resourceName.equals("")) {
                                readResourceData(dirPath+SEPARATOR+resourceName);
                                ResponseDataModel rdm = readResourceData(dirPath+SEPARATOR+resourceName);

                                responseWriterLogger("json type detected");
                                responseWriterLogger("json object ="+data);

                                print(rdm,response);
                                return;
                            }
                        }
                    }

                    ResponseDataModel rdm = gerResource(dirPath,configFile,paramList,paramValueList);
                    print(rdm,response);

                } else {
                    getOrCreateDirectory(dirPath);
                    getOrCreateFileData(dirPath+SEPARATOR+CONFIG_FILE);
                    String message = resourceNotFoundMessageGenerator(dirPath);
                    resourceNotFound(response,message);
                }
            }
        }



    }

    private ResponseDataModel gerResource(String directoryPath, List<String> configFile, List<String> params , List<String> paramsValue) throws IOException {
        if(params.size() != paramsValue.size()) {
            return new ResponseDataModel(500,
                    "Oops, Something went wrong!!!<br/>number of parameters and parameters values don't match",
                    "text/html");
        }

        String filename = "";

        List<String> paramsKeeper = new ArrayList<>();

        boolean reachedSeparator = true;
        for(String line : configFile) {
            if(line.equals(KEYWORD_SEPARATOR)) {
                reachedSeparator = true;
                if(params.size()==0 && paramsKeeper.size()==0 && (! filename.equals(""))) {
                    return readResourceData(directoryPath+SEPARATOR+filename);
                }
                if(doesParamsMatch(params,paramsValue,paramsKeeper)) {
                    return readResourceData(directoryPath+SEPARATOR+filename);
                } else {
                    paramsKeeper.clear();
                    filename ="";
                }
            } else if(reachedSeparator) {
                filename = line;
                reachedSeparator = false;
            } else {
                paramsKeeper.add(line);
            }
        }
        return new ResponseDataModel(404,
                "resource not found",
                "text/html");

    }
    private ResponseDataModel readResourceData(String path) throws IOException {
        ResponseDataModel rdm = new ResponseDataModel();
        if(isFileOrDirectoryExist(path)) {
            rdm.setContent(readFromFile(path));
            if(isFileOrDirectoryExist(path+CONFIG_FILE_HEADER)) {
                List<String> temp = readFromFile(path+CONFIG_FILE_HEADER);

                for(String s: temp) {
                    System.out.println(s);
                }

                List<String> headers = new ArrayList<>();
                List<String> values = new ArrayList<>();

                if(temp.size()%2!=0) temp.add("");

                for(int i =0 ; i<temp.size()/2; i++){
                    headers.add(temp.get(i*2));
                    values.add(temp.get(i*2+1));
                }

                rdm.setCustomHeader(headers);
                rdm.setCustomHeaderValues(values);
            }
            if(path.endsWith(".json")) {
                rdm.setContentType("application/json");
            } else  if(path.endsWith(".xml")) {
                rdm.setContentType("application/xml");
            } else {
                rdm.setContentType("text/html");
            }
            rdm.setStatus(200);
        } else {
            rdm.setStatus(404);
            rdm.setContentType("text/html");
            rdm.setErrorMessage(path + " does not exist! please create it");
        }
        return rdm;
    }
    private boolean doesParamsMatch(List<String> params , List<String> paramsValue, List<String> readerParams){
        if((params.size()*2)!=readerParams.size()) return false;
        int numberOfMatchElements = 0;
        for(int i = 0; i <params.size(); i++ ) {
            String expectedParameter = params.get(i);
            String expectedValue = paramsValue.get(i) != null ? paramsValue.get(i) : "*";
            for(int j =0; j < readerParams.size()/2; j++) {
                if(readerParams.get(j*2).equals(expectedParameter)) {
                    if(readerParams.get(j*2+1).equals("*") ||
                            readerParams.get(j*2+1).equals(expectedValue))
                    { numberOfMatchElements++;}
                    else { break; }
                }
            }
        }
        return numberOfMatchElements == params.size();
    }


    private void print(ResponseDataModel rdm, HttpServletResponse response) throws IOException {
        PrintWriter out = response.getWriter();
        if(rdm.getStatus()==200) {

            if(this.response_delay) {
                try {
                    responseWriterLogger("sleeping for "+ this.response_delay_time_ms);
                    Thread.sleep((long) this.response_delay_time_ms);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            if(this.response_custom_status) {
                responseWriterLogger("Status = "+this.response_custom_status_value);
                response.setStatus(this.response_custom_status_value);
            }

            response.setContentType(rdm.getContentType());
            for(String s: rdm.getContent()) {
                responseWriter(out,s);
            }
            for(int i =0; rdm.getCustomHeader()!=null && i < rdm.getCustomHeader().size(); i++) {
                response.setHeader(rdm.getCustomHeader().get(i),rdm.getCustomHeaderValues().get(i));
            }
        } else if (rdm.getStatus() == 404) {
            resourceNotFound(response,rdm.getErrorMessage());
        } else if (rdm.getStatus() == 500) {
            responseWriter(out,rdm.getErrorMessage());

            response.setStatus(500);
            response.setContentType(rdm.getContentType());
        } else {
            response.setContentType(rdm.getContentType());
            for(String s: rdm.getContent()) {
                responseWriter(out,s);
            }
            for(int i =0; i < rdm.getCustomHeader().size(); i++) {
                response.setHeader(rdm.getCustomHeader().get(i),rdm.getCustomHeaderValues().get(i));
            }
        }
    }

    private void responseWriter(PrintWriter out, String message) throws IOException {
        out.println(message);
        responseWriterLogger(message);
    }
    private void responseWriterLogger(String message) throws IOException {
        System.out.println(message);
        appendToLogger(message);
    }

    private String resourceNotFoundMessageGenerator(String dirPath) {
        return  "<h1>resource not found</h1><br/>"+
                "<p class=\"output\">folder : <b>"+dirPath+"</b> <em>not exist</em></p>"+
                "<br/>"+
                "<h2>it has been created for you</h2><br/>"+
                "<br/>"+
                "all you need to do is go to \""+dirPath+"\"<br/>"+
                "<br/>"+
                "<b><em>first copy your response file (.json / .xml) in the folder</em></b><br/>"+
                "and then open the '"+CONFIG_FILE+"' file<br/>"+
                "<br/>"+
                "<div class=\"black\">"+
                "append it as follow<br/>"+
                "<br/>"+
                "<ol>"+
                "<li>first line is name of your response file ex. 'something.json' <em> without quotations </em></li>"+
                "<li>second line is <b>name of your parameter </b> witch you want to check for this url</li>"+
                "<li>third line is the <b>value of your parameter</b> witch you want to match the parameter - use asterisk '*' to match any value</li>"+
                "<li>if you have more than one parameter repeat step 2 and 3</li>"+
                "<li>if you <b>don't have any parameter checking</b> just skip the 2nd and 3rd steps</li>"+
                "<li>finally add '--' <em> (without quotations) </em> at the next line as termination symbol</li>"+
                "</ol>"+
                "<br/>"+
                "Ex.<br/>"+
                "<hr>"+
                "somefile.xml<br/>"+
                "q1<br/>"+
                "valueOfQ1<br/>"+
                "q2<br/>"+
                "some other value<br/>"+
                "--<br/>"+
                "anotherfile.json<br/>"+
                "name<br/>"+
                "jackson<br/>"+
                "age<br/>"+
                "25<br/>"+
                "zipcode<br/>"+
                "12345<br/>"+
                "--<br/>"+
                "yetanotherfile.json<br/>"+
                "--<br/>"+
                "lastfile.xml<br/>"+
                "gender<br/>"+
                "*<br/>"+
                "--<br/>"+
                "<hr>"+
                "</div>";
    }
    private void resourceNotFound(HttpServletResponse response, String message) throws IOException {
        PrintWriter out = response.getWriter();
        response.setContentType("text/html");
        response.setStatus(404);
        out.println("<!DOCTYPE html>");
        out.println("<html>");
        out.println("<head>");
        out.println(" <link rel=\"stylesheet\" type=\"text/css\" href=\"/static/default.css\">");
        out.println("<meta charset=\"UTF-8\">");
        out.println("<title>404 - read the instruction below</title>");
        out.println("</head>");
        out.println("<body>");
        out.println("<div class=\"noise\"></div>");
        out.println("<div class=\"overlay\"></div>");
        out.println("<div class=\"terminal\">");
        out.println("<h1>Error <span class=\"errorcode\">404</span></h1>");
        out.println(message);
        out.println("</div>");
        out.println("</body>");
        out.println("</html>");
    }

    private void initFromConfigFile() throws IOException {
        List<String> lines = readConfigFile();

        boolean delayDetected = false , delayValue = false, customErrorDetected = false , errorValue = false;
        for(String line : lines) {
            if(delayDetected) {
                delayDetected = false;
                if(line.equals(ENABLE_KEYWORD)) this.response_delay = true;
            }

            if(customErrorDetected) {
                customErrorDetected = false;
                if(line.equals(ENABLE_KEYWORD)) this.response_custom_status = true;
            }

            if(delayValue) {
                delayValue = false;
                this.response_delay_time_ms = Integer.parseInt(line);
            }

            if(errorValue) {
                errorValue = false;
                this.response_custom_status_value = Integer.parseInt(line);
            }

            if(line.equals(RESPONSE_DELAY_KEYWORD)) delayDetected = true;
            if(line.equals(RESPONSE_DELAY_TIME_KEYWORD)) delayValue = true;
            if(line.equals(RESPONSE_CUSTOM_STATUES_KEYWORD)) customErrorDetected = true;
            if(line.equals(RESPONSE_CUSTOM_STATUES_VALUE_KEYWORD)) errorValue = true;

        }
    }
}
