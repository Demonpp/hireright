package helper;

import server.Emulator;

import java.io.*;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static server.FilesPath.*;


public class IOHelper {
    public static String getCssResource() {

        URL cssUrl = Emulator.class
                .getClassLoader().getResource("static/stylesheets/default.css");
        return cssUrl.getPath();
    }
    public static File getRootFolder() {
        try {
            File root;
            String runningJarPath = Emulator.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath().replaceAll("\\\\", "/");
            int lastIndexOf = runningJarPath.lastIndexOf("/target/");
            if (lastIndexOf < 0) {
                root = new File("");
            } else {
                root = new File(runningJarPath.substring(0, lastIndexOf));
            }
            return root;
        } catch (URISyntaxException ex) {
            throw new RuntimeException(ex);
        }
    }
    public static void writeToFile(String path, String[] values) throws IOException {
        File file = new File(path);
        FileOutputStream fos = new FileOutputStream(file);
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
        for (String v: values) {
            bw.write(v);
            bw.newLine();
        }
        bw.close();
    }
    public static List<String> readFromFile(String path) throws IOException {
        List<String> fileLines = new ArrayList<>();
        BufferedReader reader;
        reader = new BufferedReader(new FileReader(path));
        String line = reader.readLine();
        while (line != null) {
            fileLines.add(line);
            line = reader.readLine();
        }
        reader.close();
        return fileLines;
    }
    public static File getOrCreateDirectory(String path) {
        File dir = new File(path);
        if(!dir.exists()) dir.mkdir();
        return dir;
    }
    public static Path createTempDirectory(String path) throws IOException {
        return Files.createTempDirectory(path);
    }

    public static List<String> readConfigFile() throws IOException {
        System.out.println(getRootFolder().getAbsolutePath()+SERVER_CONFIGURATION_DIRECTORY+SERVER_FILE);
        return  getOrCreateFileData(getRootFolder().getAbsolutePath()+SERVER_CONFIGURATION_DIRECTORY+SERVER_FILE,null);
    }


    public static List<String> getOrCreateFileData(String path) throws IOException {
        return  getOrCreateFileData(path,null);
    }
    public static List<String> getOrCreateFileData(String path, String[] initialValues) throws IOException {
        File file = new File(path);
        if(!file.exists()) {
            file.createNewFile();

            if(initialValues == null) return new ArrayList<>();

            writeToFile(file.getAbsolutePath(),initialValues);
            return Arrays.asList(initialValues);
        } else {
            List<String> fileLines = readFromFile(path);
            return fileLines;
        }
    }
    public static List<String> forceToCreateFileData(String path, String[] initialValues) throws IOException {
        File file = new File(path);

        if(file.exists()) {
           file.delete();
        }
        file.createNewFile();

        if(initialValues == null) return new ArrayList<>();

        writeToFile(file.getAbsolutePath(),initialValues);
        return Arrays.asList(initialValues);
    }
    public static Boolean isFileOrDirectoryExist(String path) {
        File fileOrDir = new File(path);
        return fileOrDir.exists();
    }
    public static void appendToLogger(String message ) throws IOException {
        File file = new File(getRootFolder().getAbsolutePath()+LOG_FILE);
        FileWriter fr = new FileWriter(file, true);
        BufferedWriter br = new BufferedWriter(fr);
        br.write(message);
        br.newLine();

        br.close();
        fr.close();
    }

}
