package helper;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.MessageDigest;

import static helper.IOHelper.getRootFolder;
import static server.FilesPath.*;
import static server.FilesPath.SEPARATOR;

public class Util {
    public static int tryParse(String value, int defaultVal) {
        try {
            return Integer.parseInt(value);
        } catch (NumberFormatException e) {
            return defaultVal;
        }
    }
    public static Boolean tryParse(String value, String defaultVal,Boolean proceedAsEnableDisable) {
        if(proceedAsEnableDisable)
            return value.toLowerCase().equals(ENABLE_KEYWORD);
        return  value.toLowerCase().equals("true");
    }
    public static String convertUriToPath(String s, String requestMethod) {
        // calculate relative path based on method request
        String middlePath = "";
        if(requestMethod.equals("post")) {
            middlePath = POST_REQUEST_DIRECTORY_RELATIVE_PATH;
        } else {
            middlePath = GET_REQUEST_DIRECTORY_RELATIVE_PATH;
        }

        if(!s.equals("/")) {
            // to count forward slashes
            int cnt = 0;
            int n = s.length();
            char ch[] = s.toCharArray();
            int res_ind = 0;

            for (int i = 0; i < n; i++)
            {

                // check for forward slash in the uri
                if (ch[i] == '/')
                {
                    cnt++;
                    // conversion into upper case
                    ch[i + 1] = Character.toUpperCase(ch[i + 1]);
                    continue;
                }

                // If not space, copy character
                else
                    ch[res_ind++] = ch[i];
            }

            // new string will be reduced by the
            // size of forward slashes in the original uri
            // and append the physical path
            return getRootFolder().getAbsolutePath()+ middlePath + SEPARATOR +String.valueOf(ch, 0, n - cnt);
        } else  {
            return getRootFolder().getAbsolutePath()+ middlePath ;
        }

    }
    public static String getFileChecksum(MessageDigest digest, File file) throws IOException {
        //Get file input stream for reading the file content
        FileInputStream fis = new FileInputStream(file);

        //Create byte array to read data in chunks
        byte[] byteArray = new byte[1024];
        int bytesCount = 0;

        //Read file data and update in message digest
        while ((bytesCount = fis.read(byteArray)) != -1) {
            digest.update(byteArray, 0, bytesCount);
        };

        //close the stream; We don't need it now.
        fis.close();

        //Get the hash's bytes
        byte[] bytes = digest.digest();

        //This bytes[] has bytes in decimal format;
        //Convert it to hexadecimal format
        StringBuilder sb = new StringBuilder();
        for(int i=0; i< bytes.length ;i++)
        {
            sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
        }

        //return complete hash
        return sb.toString();
    }
}
