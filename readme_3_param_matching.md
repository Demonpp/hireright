<h2>param matching for GET/POST request</h2><br/>
<p>in 'config.txt' file you can specify some rules so system can match request with specific param with appropriate response file</p><br/>
<hr><br/>
<h3>steps</h3><br/>
<li> 1 - open 'config.txt' file and append the following</li><br/>
<li> 2 - first line should be name of file witch you wish to return</li><br/>
<li> 2 - 1 - ** it should have (.json/.xml) extension</li><br/>
<li> 2 - 2 - ** this file should be placed in same directory as 'config.txt'</li><br/>
<li> 3 - in the next line write name of parameter you want to match and press Enter</li><br/>
<li> 4 - and in the write the value of that parameter you wish to match</li><br/>
<li> 4 - 1 use asterisk '*' without quotas to match any</li><br/>
<li> 5 - if you have more than ONE parameter that you need to match repeat from step 3</li><br/>
<li> 5 - 1 if you dont have any parameters skip step 3,4 and five, and go to step 6</li><br/>
<li> 6 - finally enter double dashes '--' without quotas as indicator of end of rule</li><br/>
<li> 7 - **if you have more rules go to step 1 and repeat the steps</li><br/>
<hr><br/>
ex.<br/>
<hr><br/>
<pre>
hi.json
--
hi2.xml
q
8
--
hi3.xml
q
8
p
9
--
</pre>


